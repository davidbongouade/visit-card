import 'package:flutter/material.dart';

const nomCarteDeVisite = 'David';
const statutcarteDeVisite = "Entrepreneur et Developpeur\n Mobile Flutter";
const textbuttonCarteDeVisite = "En savoir plus";
const descripCarteDeVisite =
    "Pendant mon parcours scolaire, je suis passé de l'électronique à l'électrotechnique, une fois l'école terminée j'ai pris la décision de me reconvertir en développeur d'applications car passionné d'informatique depuis tout petit. Ma reconversion n'a guère été difficile à cause de mes aptitudes innées pour l'informatique et aujourd'hui je suis développeur Mobile Flutter bien que je continue d'apprendre.";
const emailCarteDeVisite = "bongouade@gmail.com";
const colorBackground = 0xff052555;

const styleNomCarteDeVisite = TextStyle(
  color: Colors.white,
);
const styleStatuCarteDeVisite = TextStyle(
  color: Colors.white,
);
const styleButtonCartedeVisite = TextStyle(
  color: Colors.white70,
);
const styleDescripCarteDeVisite = TextStyle(
  color: Colors.white,
);
const styleEmailCarteDeVisite = TextStyle(
  color: Colors.white,
);
